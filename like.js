    function addToLiked() {
        let thing = document.getElementById("thing").value;
        let liked = document.getElementById("liked");
        let li = document.createElement("li");
        li.appendChild(document.createTextNode(thing));
        liked.appendChild(li);
        document.getElementById("thing").value = "";
    }

    function addToDisliked() {
        let thing = document.getElementById("thing").value;
        let disliked = document.getElementById("disliked");
        let li = document.createElement("li");
        li.appendChild(document.createTextNode(thing));
        disliked.appendChild(li);
        document.getElementById("thing").value = "";
    }

    function clearLists() {
        document.getElementById("liked").innerHTML = "";
        document.getElementById("disliked").innerHTML = "";
    }

    